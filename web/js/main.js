(function () {
    
    //Datatable inistilised
    $('#dataTable1').DataTable({
        dom:'t',
        responsive: true,
        "columnDefs": [ {

            "targets":  'no-sort',
            "orderable": false

        } ]
    });

    //tale row selected effect
    $('.tst-user-check').on('change',function(){

        var $this = $(this);
        if($this.prop('checked')){
            
            $this.parents('tr').addClass('selected');

        }else{
            
            $this.parents('tr').removeClass('selected');

        }

    });


    //add class on tab change
    $('.tst-tabs-wrapper [data-toggle="tab"]').on('shown.bs.tab', function(){ 

        setTimeout(function(){

            $('.tab-pane').removeClass('selected');
            $('.tab-pane.active').addClass('selected');

        },100)

    });

    $('.tst-tabs-wrapper [data-toggle="tab"]').on('hide.bs.tab', function(){ 
 
        $('.tab-pane:not(.active)').addClass('removed');  

    });
 

    //on scroll change search animation
    var targetOffset = $(".tst-welcome-slider-wrapper").offset().top + 50;
     $(window).scroll(function(){
         console.log($(document).scrollTop())
        if ( $(document).scrollTop() > 400 ) {   

            $(".tst-input-wrapper").addClass('shown') 

        }  
    });

})();